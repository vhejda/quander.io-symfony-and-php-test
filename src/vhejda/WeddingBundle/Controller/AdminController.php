<?php

namespace vhejda\WeddingBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     * @param Request $request
     * @return Response|RedirectResponse
     */
    public function adminAction(Request $request)
    {
        /** @var ObjectManager $manager */
        $manager = $this->getDoctrine()->getManager();

        // WeddingType form (registered as a service):
        $form = $this->createForm("wedding", null, array(
            "label" => false,
            "action" => $this->generateUrl('admin'))
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() AND $form->isValid()) {
            $wedding = $form->getData();
            $manager->persist($wedding);
            $manager->flush();

            $this->addFlash('info', 'New wedding created!');

            return $this->redirectToRoute("admin");
        }

        // All existing weddings:
        $weddings = $manager->getRepository("WeddingBundle:Wedding")->findAll();

        return $this->render('admin/admin.html.twig', array(
            'form' => $form->createView(),
            'weddings' => $weddings
        ));
    }

    /**
     * @Route("/admin/delete/{id}", name="admin_delete_wedding")
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteWeddingAction($id)
    {
        /** @var ObjectManager $manager */
        $manager = $this->getDoctrine()->getManager();

        $wedding = $manager->getRepository("WeddingBundle:Wedding")->find($id);

        if ($wedding) {
            $manager->remove($wedding);
            $manager->flush();
            $this->addFlash('info', "Wedding with id $id was succesfully deleted");
        }else{
            $this->addFlash('info', "Wedding with id $id does not exist");
        }

        return $this->redirectToRoute("admin");
    }


}
