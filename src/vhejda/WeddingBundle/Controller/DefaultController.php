<?php

namespace vhejda\WeddingBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function indexAction()
    {
        return new Response();
    }

    /**
     * @Route("/wedding/{slug}", name="wedding_detail")
     * @param string $slug
     * @return Response
     */
    public function weddingDetailAction($slug)
    {
        return new Response();
    }
}
