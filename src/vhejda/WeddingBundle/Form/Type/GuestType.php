<?php

namespace vhejda\WeddingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeddingType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' =>'vhejda\WeddingBundle\Entity\Wedding',
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                "required" => true,
                "label" => "Name of the wedding"
            ))
            ->add('address', 'text', array(
                "required" => true,
                "label" => "Address"
            ))
            ->add('save', 'submit', array(
                "label" => "Save"
            ));
    }

    public function getName()
    {
        return "wedding";
    }

}