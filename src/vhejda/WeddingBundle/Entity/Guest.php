<?php

namespace vhejda\WeddingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 *
 * @author Vojtěch Hejda <vojtech.hejda@gmail.com>
 */
class Guest
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int $id
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $name
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @var string $message
     */
    private $message;

    /**
     * @ORM\Column(type="string")
     * @var string $name
     */
    private $photo;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime $createdAt
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="Wedding", inversedBy="guests")
     * @ORM\JoinColumn(name="wedding_id", referencedColumnName="id")
     * @var Wedding $wedding
     */
    private $wedding;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Guest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Guest
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Guest
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Guest
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Doctrine LifeCycle callback
     *
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->created = new \DateTime();
    }

    /**
     * Set wedding
     *
     * @param \vhejda\WeddingBundle\Entity\Wedding $wedding
     * @return Guest
     */
    public function setWedding(Wedding $wedding = null)
    {
        $this->wedding = $wedding;

        return $this;
    }

    /**
     * Get wedding
     *
     * @return \vhejda\WeddingBundle\Entity\Wedding 
     */
    public function getWedding()
    {
        return $this->wedding;
    }


}
