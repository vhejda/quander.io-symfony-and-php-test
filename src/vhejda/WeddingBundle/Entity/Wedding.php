<?php

namespace vhejda\WeddingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity
 *
 * @author Vojtěch Hejda <vojtech.hejda@gmail.com>
 */
class Wedding
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int $id
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $name
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @var string $address
     */
    private $address;

    /**
     * Slug created from $name using Gedmo Sluggable
     *
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @var string $slug
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Guest", mappedBy="wedding", cascade={"persist", "remove"})
     * @var ArrayCollection $guests
     */
    private $guests;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->guests = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Wedding
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Wedding
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add guests
     *
     * @param \vhejda\WeddingBundle\Entity\Guest $guests
     * @return Wedding
     */
    public function addGuest(\vhejda\WeddingBundle\Entity\Guest $guests)
    {
        $this->guests[] = $guests;

        return $this;
    }

    /**
     * Remove guests
     *
     * @param \vhejda\WeddingBundle\Entity\Guest $guests
     */
    public function removeGuest(\vhejda\WeddingBundle\Entity\Guest $guests)
    {
        $this->guests->removeElement($guests);
    }

    /**
     * Get guests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGuests()
    {
        return $this->guests;
    }
}
